const { app, BrowserWindow, ipcMain, dialog } = require('electron')
const auth = require('./auth.js') // подключаем скрипт для авторизации
const StringBuilder = require('node-stringbuilder'); // Билдер Replace
const ioHook = require('iohook') // подключаем библиотеку глобального хука
 
// Создание окна Chrome 
function createWindow () {

	const mainWindow = new BrowserWindow({width:800,height:600,show: false})

	const authWindow = new BrowserWindow({
		title: "JMU Hostels",
		width: 440,
		height: 430,
		frame: false,
		alwaysOnTop: false, 
		hasShadow: false,
		transparent: true,
		resizable: false,
		webPreferences: {
			nodeIntegration: true,
			enableRemoteModule:true,
		}
	})

	
	//authWindow.maximize();

	// Для Debug
	authWindow.webContents.openDevTools()

	// Переход по url react 
	authWindow.loadURL('http://localhost:3000/auth')

	authWindow.removeMenu()
	authWindow.setMenu(null)

	ipcMain.on('error-sign-in', (event, message, detail, title) => {
		dialog.showMessageBox(authWindow, {
			type:'error',
			title,
			message,
			detail,
		})
	})

	ipcMain.on('entry-accepted', (event, arg) => {
		if(arg=='ping'){
			mainWindow.show()
			mainWindow.loadURL('http://localhost:3000/main')
			authWindow.hide()
		}
	  })
}

app.whenReady().then(createWindow)
app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

// Записываем данные в случае при событии keydown
ioHook.on('keydown', (event) => {
	//console.log(event); // { type: 'mousemove', x: 700, y: 400 }
  });
  
// Запуск хука 
ioHook.start();

// При активации создать окно
app.on('activate', () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		createWindow()
	}
})

auth()
