const { ipcMain, session  } = require("electron")
const rbjs = require("random-bytes-js")
const CryptoJS = require("crypto-js")
const axios = require("axios")
require('dotenv').config()


const  signIn = () => {
	return ipcMain.on('auth', (event, login, password) => {
		const key = process.env.CRYPTO_KEY;
		let iv = ''
		iv = rbjs.rand64(32);

		if(iv.length > 32)
			iv = iv.slice(0,32);

		const AesKey = CryptoJS.enc.Utf8.parse(key);
		const byteIv = CryptoJS.enc.Hex.parse(iv);
		const encryptedStringHex = CryptoJS.AES.encrypt(password, AesKey, {
			iv: byteIv,
			mode: CryptoJS.mode.CBC,
			format: CryptoJS.format.Hex
		}).ciphertext;
		axios
		.post(`${process.env.API_URL}/api/auth`, {
			login,
			password: CryptoJS.enc.Hex.stringify(byteIv) + ':' + encryptedStringHex.toString(CryptoJS.enc.Hex),
			id_module: 3,
		})
		.then((req) => {
			if (req && req.data && req.data.auth_token) {
				console.log(req.data);
				const cookie = { url: process.env.DEV_URL, name: 'auth-token', value: req.data.auth_token }
				session.defaultSession.cookies.set(cookie)
				axios.defaults.headers.common['auth-token'] = req.data.auth_token
				
				event.reply(
					'auth-reply',
					req.data,
					req.status,
					req.headers
				)
			}
		})
		.catch((error) => {
			console.log(error);
			if(error.response) {
				event.reply(
					'auth-reply',
					error.response.data,
					error.response.status,
					error.response.headers
				);
			}	
		});

	})
}

module.exports = signIn
