import React, {useState} from 'react';
import Logo from "./Logo";
import { Button } from 'react-bootstrap';

const { ipcRenderer , remote  } = window.require('electron');

/*async function post(login, password) {

	const key = '8UHjPgXZzXDgkhqV2QCnooyJyxUzfJrO';
		let iv = ''

		iv = rbjs.rand64(32);

		if(iv.length > 32)
			iv = iv.slice(0,32);

		const AesKey = CryptoJS.enc.Utf8.parse(key);
		const byteIv = CryptoJS.enc.Hex.parse(iv);
		const encryptedStringHex = CryptoJS.AES.encrypt(password, AesKey, {
			iv: byteIv,
			mode: CryptoJS.mode.CBC,
			format: CryptoJS.format.Hex
		}).ciphertext;

		await axios.get('https://jsonplaceholder.typicode.com/todos/1', {
			login,
			password: CryptoJS.enc.Hex.stringify(byteIv) + ':' + encryptedStringHex.toString(CryptoJS.enc.Hex),
			id_module: 3,
		},
		{
			headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*'
			}
		}).then((response) => {
			if(response.status === 200) {
				window.location.href = '/main';

			}
		  });		
}*/

function Auth() {
	const [login, setLogin] = useState('');
	const [password, setPassword] = useState('');

	// We call the authorization function when clicking inter on the fields of the form
	const validateKeyPress = (key) => {
		if (key === 'Enter') signIn();
	}

	// Validation of the entered data and request to the server
	const signIn = () => {
		const log = login.trim();
		const pass = password.trim();
		if (log && pass) {
			//post(log, pass);
			ipcRenderer.send('auth', login, password)
		}
		ipcRenderer.on('auth-reply', (event, data, status, header) => {
			if (status === 401) {
				error('Ошибка авторизации', data.resp.ru, 'Ошибка:' + status);
			}
			else if (status === 200) {

				/*let win = new remote.BrowserWindow({
					parent: remote.getCurrentWindow(),
					modal: true
				  })
				win.loadURL('https://github.com');
				*/
				ipcRenderer.send('entry-accepted', 'ping')
				 
				//window.location.href = 'https://github.com';
				//return <Redirect to='https://github.com'/>;
			}
		});
	}

	const error = (message, detail, title) => {
		ipcRenderer.send('error-sign-in', message, detail, title);
	}

	return (
		<div className="auth-form shadow-sm d-flex flex-column">
			<div className="d-flex align-items-center justify-content-center w-100">
				<Logo />
			</div>
			<div className="w-100 d-flex flex-column justify-content-center px-2">
				<div className="row px-4">
					<label htmlFor="">Идентификатор:</label>
					<input
						type="text"
						className="form-control"
						placeholder="Ваш id в системе"
						value={login}
						onChange={event => setLogin(event.target.value)}
						onKeyPress={event => validateKeyPress(event.key)}
						maxLength="100"
					/>
				</div>
				<div className="row px-4 mt-3">
					<label htmlFor="">Пароль:</label>
					<input
						type="password"
						className="form-control"
						placeholder="Ваш пароль"
						value={password}
						onChange={event => setPassword(event.target.value)}
						onKeyPress={event => validateKeyPress(event.key)}
						maxLength="100"
					/>
				</div>
			</div>
			<div className="w-100 d-flex justify-content-between align-items-center px-4">
				<p className="m-0">Alpha 1.4.8</p>
				<Button variant="primary" className="px-4" onClick={signIn}>Войти</Button>
			</div>
		</div>
	);
};

export default Auth;
