import React from 'react';

function Logo() {
	return (
		<div className="logo">
			<h3>JMU</h3>
			<h5 className="px-3 m-0">Hostels</h5>
		</div>
	);
};

export default Logo;
