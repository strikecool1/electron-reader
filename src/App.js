import './styles/App.scss';
import Auth from './components/Auth';
import Main from './components/Main';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

/*function setToken(userToken) {
  sessionStorage.setItem('token', JSON.stringify(userToken));
}

function getToken() {
  const tokenString = sessionStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  return userToken?.token
}
*/

function App() {
  return (
      <div className="container-fluid d-flex align-items-center justify-content-center mh-100vh">
        <BrowserRouter>
        <Switch>
          <Route path="/auth">
              <Auth/> 
          </Route>
          <Route path="/main">
              <Main/> 
          </Route>
        </Switch>
        </BrowserRouter>
          

      </div>
  );
}

export default App;
